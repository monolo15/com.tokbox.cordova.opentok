OpenTok Android SDK release notes
=================================

New features and changes
------------------------

Version 2.9.0

* This version adds 64-bit support (the armeabi64-v8a architecture).

* The OpenTok Media Router now includes the OpenTok scalable video feature
  (previously in beta). For more information, see the
  [OpenTok Media Router](https://tokbox.com/developer/guides/create-session/#media-mode)
  documentation.

* This version adds a `Session.getCapabilities()` method. Use this method
  to check whether the client can publish streams to an OpenTok session,
  based on the role assigned to the token used to connect to the session.

* Please upgrade your apps to use the latest versions of the [OpenTok server
  SDKs](https://tokbox.com/developer/sdks/server/). These now use JSON web
  tokens (JWT) for authentication. JWT provides increased security along with
  standards-based encoding, decoding, and verification. The old form of
  authentication will expire in July 2017. Also, if you use the OpenTok REST API
  (instead of the server SDKs), please use JWT to authenticate calls to the
  OpenTok REST methods (see [this topic](https://tokbox.com/developer/rest/#authentication)
  on authentication).

Version 2.8.2

* Fixed bug where the publisher audio-video state was not preserved between application
  lifecycle events.

* Fixed a potential memory leak in subscribers.

* Fixed a Bluetooth routing issue on certain devices.

* Fixed bug in which custom video renderers resulted in black videos.

Version 2.8.1

* This version fixes an audio-video synchronization issue in version 2.8.0.

* Note that we have added TLS support to OpenTok TURN servers. This fixes
  media streaming for clients behind network firewalls that block non-TLS
  traffic over port 443.

Version 2.8.0

* The library is now provided as an aar file (not a jar file).

* This version add support for Bluetooth SCO profile devices (earpieces and headsets) in the default
  audio driver.

* This version improves quality for published audio.

* The new `Publisher.startPreview()` method lets you display a publisher's camera video in the
  Publisher's view before it starts streaming video.

* The new `Session.SessionOptionsProvider` interface lets you decide when to use hardware video
  decoding.

* The `Publisher.swapCamera()`, `Publisher.getCameraId()`, and `Publisher.setCameraId()` methods
  are deprecated. You should use the new `Publisher.cycleCamera()` method to cycle between cameras,
  if there are multiple cameras on the device. Use the new `BaseVideoCapturer.CaptureSwitch`
  interface to define the behavior of the `Publisher.cycleCamera()` method in a custom video
  capturer.

* The `PublisherKit.setCapturer(BaseVideoCapturer capturer)` method is deprecated. Instead, use the
  `PublisherKit.capturer` property when creating a custom PublisherKit implementation. Or use the
  new `Publisher(Context context, String name, BaseVideoCapturer capturer)` constructor.

* This version of the SDK uses WebRTC 49.

Version 2.7.0

* Setting the frame rate and resolution for a published stream using the default camera video
  capturer. A new version of the Publisher constructor, `Publisher(context, name, resolution, fps)`,
  includes parameters for setting the frame rate and resolution of the stream.

* New methods for getting audio and video statistics for a stream. You can now set listeners to
  monitor the following statistics for a subscriber's stream:

  * Total audio and video packets lost
  * Total audio and video packets received
  * Total audio and video bytes received

  See the documentation for the `SubscriberKit.setAudioStatsListener(AudioStatsListener listener)`
  and `SubscriberKit.setVideoStatsListener(VideoStatsListener listener)` methods.

* Added support for armeabi-v7a.

* This version requires Android 4.1 (Jelly Bean, API Level 16) or later.

Version 2.6.0

* Added the `Session.reportIssue()` method. You can call this method when your app
  experiences an issue. The method returns an issue ID, which you can use with the
  <a href="https://tokbox.com/developer/tools/Inspector">Inspector</a> or when
  discussing an issue with the TokBox support team.

* This release fixes the following issues:

  * Subscribers did not display video when the stream changed from audio-only to audio-video.

  * Calling the `Session.sendSignal()` method when not connected to the session resulted in
    the app crashing.

  * Calling the `getData()` method of the Connection object passed into the
    `Session.SignalListener.onSignalReceived(session, type, data, connection)` method did
    not return connection data.

  * When network connections were re-routed through a proxy or when Session objects were
    garbage collected, fatal signal 11 (SIGSEGV) errors would occur..

  * Reference table overflow errors caused apps to crash.

Version 2.5.0

* Added proxy support.

* Note that the samples directory is no longer included in the SDK bundle. The sample code is now
  available at the open-source
  [opentok-android-sdk-samples repository](https://github.com/opentok/opentok-android-sdk-samples)
  on GitHub. This allows us to keep it up to date and provide developers with latest version of
  the sample code. Feel free to clone the repo or download the source code to see the best-practice
  examples of OpenTok usage.

* Added hardware-accelerated VP8 decoding of videos on the Nexus 5 with Android Lollipop+.

* Fixed a bug that could occur when you reuse a Session object to reconnect to a session.

* Added a `SessionIllegalState` error in the `OpentokError.ErrorCode` enum. This defines an error
  code for an error that occurs if you try to connect to a session that is already connected or if
  you try to subscribe to a stream that is no longer in the session.

Version 2.4.1

* Fixed an issue with failing DTLS negotiations with newer versions of Firefox,
  which can cause publishing and subscribing to fail in relayed OpenTok sessions
  (sessions that do not use [the OpenTok Media
  Router](http://tokbox.com/opentok/tutorials/create-session/#media-mode ) ).

Version 2.4.0

* This version adds support for testing on the Genymotion x86 emulator and the official Android x86
  emulator images in combination with the Intel HAXM software for VM Acceleration.

* This version adds support for screen sharing. When publishing a screen-sharing stream,
  call the `setPublisherVideoType(type)` method of the PublisherKit object and pass in
  `PublisherKitVideoType.PublisherKitVideoTypeScreen`. This optimizes the video encoding
  for screen sharing. It is recommended to use a low frame rate (5 frames per second or lower)
  with screen sharing. 	When using the
  screen video type in a session that uses the OpenTok Media Server, you should
  call the `setAudioFallbackEnabled(boolean enabled)` method of the Publisher object and
  pass in <code>true</code> to disable the audio-only fallback feature, so that the
  video does not drop out in subscribers. See [the OpenTok Media Router and media
  modes](http://tokbox.com/opentok/tutorials/create-session/#media-mode).

  When a stream is created in a session, you can determine the video type of the stream
  (screen or camera) by calling the `getStreamVideoType()` method of the Stream object.
  The type of stream is defined by constants in the StreamVideoType enum: `StreamVideoTypeScreen`
  and `StreamVideoTypeCamera`.

  To publish a screen-sharing stream, you need to implement a custom video capturer for the
  OTPublisherKit object. For sample code that publishes a screen-sharing stream, see the
  "screensharing" sample (in the samples directory).

* You can disable the audio-only fallback feature for a published stream by calling the
  `setAudioFallbackEnabled(enabled)` method of the PublisherKit object (passing in `false`).
  The audio-fallback feature is available in sessions that use the [OpenTok Media
  Router](http://tokbox.com/opentok/tutorials/create-session/#media-mode). With the audio-fallback
  feature enabled (the default), when the OpenTok Media Router determines that a stream's quality
  has degraded significantly for a specific subscriber, it disables the video for that subscriber
  in order to preserve audio quality. The audio-fallback feature is enabled by default.

* This version of the SDK takes advantage of hardware acceleration for video decoding on devices
  that support it. These devices include the Nexus 5 (with OS before 5.0 Lollipop), the Asus K010,
  the Asus K011, and the Asus K013.

* Updated to use BoringSSL, Google's fork of OpenSSL.

* Fixed an issue with video not recovering when network conditions improve (after video was
  disabled, due to network conditions, in a session using the OpenTok Media Server).

Version 2.3.0

* The reason parameter has been added to the
  `SubscriberKit.VideoListener.onVideoDisabled(SubscriberKit subscriber, String reason)`
  and `SubscriberKit.onVideoDisabled(SubscriberKit subscriber, String reason)`
  methods. This parameter describes the reason why the subscriber video is being
  disabled. In the previous version, this method was only called when the video
  was disabled due to changes in the stream quality (in a session that uses the
  OpenTok Media Router). In version 2.3.0, the method is also called if the
  publisher stops sending a video stream or the subscriber stops subscribing to
  it (and the `reason` parameter value will be set accordingly).

* New methods indicate when a subscriber's video stream starts (when there
  previously was no video) or resumes (after video was disabled):
  `SubscriberKit.VideoListener.onVideoEnabled(SubscriberKit subscriber, String reason)`
  and `SubscriberKit.onVideoEnabled(SubscriberKit subscriber, String reason)`.

* Use the new PublisherKit.AudioLevelListener and SubscriberKit.AudioLevelListener
  classes to monitor changes in the audio levels of publishers and subscribers.
  To use these, call the `setAudioLevelListener(listener)` method of a PublisherKit
  or SubscriberKit object.

* The new `SubscriberKit.VideoListener.onVideoDisabledWarning(subscriber)` method
  is called when the OpenTok Media Router determines that the stream quality has
  degraded and the video will be disabled if the quality degrades more. The new
  `SubscriberKit.VideoListener.onVideoDisabledWarningLifted(subscriber)` method
  is called when the stream quality improves. This feature is only available in
  sessions that use the OpenTok Media Router (sessions with the
  [media mode](http://tokbox.com/opentok/tutorials/create-session/#media-mode)
  set to routed), not in sessions with the media mode set to relayed. The
  SubscriberKit class also has protected `onVideoDisabledWarning()` and
  `onVideoDisabledWarningLifted()` methods that you can implement (instead of
  the VideoListener methods) when you subclass SubscriberKit.

* This version includes a new custom audio driver API. This lets you use
  custom audio streams and define the audio device used to capture and render
  audio data. The following new classes support the custom audio driver API:

  * AudioDeviceManager -- Use this class to set the app to specify a custom
  audio device for use in the app.

  * BaseAudioDevice -- Defines an audio device for use in a session.

  * BaseAudioDevice.AudioBus -- The audio bus marshals audio data between the network and
  the audio device.

  * BaseAudioDevice.AudioSettings -- Defines the format of the audio.

* You can optimize the speaker usage for voice-only calls by calling
  `AudioDeviceManager.getAudioDevice().setOutputMode(BaseAudioDevice.VOICE_COMMUNICATION)`.
  This sets the app to use the headset speaker (and the loudspeaker is disabled), which is
  preferable in voice-only apps.

* This build improves the video quality under poor network conditions.

* The Session.PublisherListener and the `Session.onPublisherAdded(publisher)`
  and `Session.onPublisherRemoved(publisher)` methods have been removed. You
  can monitor the `PublisherKit.PublisherListener.onStreamCreated(stream)` and
  `PublisherKit.PublisherListener.onStreamDestroyed(stream)` methods to determine
  when a publisher's stream is created and destroyed.


Version 2.2.1

* Updated to use version 1.0.1h of OpenSSL.

* The `Session.unpublish()` method now takes a PublisherKit parameter, instead of a Publisher
  parameter. (The Publisher class extends PublisherKit.)

Known issues
------------

* In a session with the [media mode](http://tokbox.com/opentok/tutorials/create-session/#media-mode)
  set to relayed, only one client can subscribe to a stream published by an Android device.

* There is a bug in the KitKat version of the Google Android native emulator that causes crashes
  when using GLSurfaceView (which is used by the OpenTok Android SDK). You can work around
  this issue by [disabling the hardware acceleration for the activity in the Android
  manifest](http://developer.android.com/guide/topics/graphics/hardware-accel.html#controlling)
  when testing in the emulator.

* Video streaming is prevented on networks that have firewalls that use authenticated
  proxies. This is due to a core issue with the current underlying WebRTC implementation.
  (See [this Chromium bug report](https://code.google.com/p/chromium/issues/detail?id=439560).)

* Audio is distorted in streams published from the Genymotion emulator.
