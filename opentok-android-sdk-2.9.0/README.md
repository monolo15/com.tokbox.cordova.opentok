OpenTok Android SDK
===================

The OpenTok Android SDK lets you use OpenTok-powered video sessions in apps you build
for Android devices.

Apps written with the OpenTok Android SDK 2.9.0 can interoperate with OpenTok apps written with the following OpenTok SDKs:

* <a href="http://tokbox.com/developer/sdks/js/">OpenTok.js 2.7+</a>
* <a href="http://tokbox.com/developer/sdks/android/">OpenTok Android SDK 2.7+</a>
* <a href="http://tokbox.com/developer/sdks/ios/">OpenTok iOS SDK 2.7+</a>

Installation
------------

The library (an aar file) is included in the root of the SDK bundle.

A Maven version is available at http://tokbox.bintray.com/maven. The artifact ID is "opentok-android-sdk".

Developer and client requirements
---------------------------------

The OpenTok Android SDK supports one published audio-video stream, one subscribed audio-video
stream, and up to five additional subscribed audio-only streams simultaneously (this is the
baseline support on a Samsung Galaxy S3). To connect more than two clients in a session using the
OpenTok Android SDK, create a session that uses the OpenTok Media Router (a session with the
media mode set to routed). See [The OpenTok Media Router and media modes](http://tokbox.com/developer/guides/create-session/#media-mode).

The SDK is supported on high-speed Wi-Fi and 4G LTE networks.

The OpenTok Android SDK is supported on armeabi, armeabi-v7a,  armeabi64-v8a, and x86 architectures.
For a list of supported devices, see the [Developer and client
requirements](http://tokbox.com/developer/sdks/android/#developer-and-client-requirements)
section of the OpenTok Android SDK page at the TokBox website.

Using the sample apps
---------------------

For sample code, go to the
[opentok-android-sdk-samples repo](https://github.com/opentok/opentok-android-sdk-samples)
at GitHub.

Creating your own app using the OpenTok Android SDK
---------------------------------------------------

The library is provided in the opentok-android-sdk-2.9.0.aar file. You no longer need to include
architecture-dependent .so files (which was required in previous versions). This file is included in
the root of the SDK package. Place the aar file into your project in Android Studio. Then modify the
gradle file for your project to reference the aar file.

A Maven version is available at http://tokbox.bintray.com/maven. The artifact ID is "opentok-android-sdk". Modify your app to download the OpenTok Android SDK from http://tokbox.bintray.com/maven. For example:

a) Edit the build.gradle for your project and add the following code snippet to the
allprojects/repositiories section:

    maven { url  "http://tokbox.bintray.com/maven" }

b) Modify build.gradle for your module and add the following code snippet to the 'dependencies' section:

    compile 'com.opentok.android:opentok-android-sdk:2.9.+'

Your app needs to use a session ID and token generated with your OpenTok API key, which you can get
by logging into your [TokBox account](https://tokbox.com/account).

For test purposes, you can generate a session ID and token by logging into your
[TokBox account](https://tokbox.com/account). For a production app, generate unique tokens
(and session IDs, if you need to support multiple sessions) using the
[OpenTok server-side libraries](https://tokbox.com/developer/sdks/server).


Permissions
-----------

The OpenTok Android SDK uses following permissions:

    android.permission.CAMERA
    android.permission.INTERNET
    android.permission.RECORD_AUDIO
    android.permission.MODIFY_AUDIO_SETTINGS
    android.permission.BLUETOOTH
    android.permission.BROADCAST_STICKY

You do not need to add these to your app manifest. The OpenTok SDK adds them automatically.
However, if you use Android 21+, certain permissions require you to prompt the user.

Documentation
-------------

Reference documentation is available in the docs subdirectory of the SDK and at <http://www.tokbox.com/developer/sdks/android/reference/index.html>.

More information
-----------------

For a list of new features and known issues, see the [release notes](release-notes.md).
